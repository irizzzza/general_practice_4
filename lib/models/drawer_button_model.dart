import 'package:flutter/foundation.dart';

class DrawerButtonModel {
  final String name;
  final String route;

  DrawerButtonModel({
    @required this.name,
    @required this.route,
  });
}
