import 'package:flutter/material.dart';

class PopNavigatorObserver extends NavigatorObserver {

  Function onPop;

  void setOnPop(Function onPop){
    this.onPop = onPop;
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic> previousRoute) {
    print('pop observer');
    if(this.onPop != null) {
      this.onPop(route, previousRoute);
    }
    super.didPop(route, previousRoute);
  }
}