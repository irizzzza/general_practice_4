import 'package:flutter/foundation.dart';
import 'package:general_practice_4/store/app_selectors.dart';
import 'package:general_practice_4/store/appstate.dart';
import 'package:redux/redux.dart';

class DrawerButtonViewModel {
      final void Function(String route) callback;

      DrawerButtonViewModel({@required this.callback});
      static DrawerButtonViewModel fromStore(Store<AppState> store) {
        return DrawerButtonViewModel(
            callback: NavigationSelector.navigateToNextPage(store)
        );
      }
}

