import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:general_practice_4/models/drawer_button_model.dart';
import 'package:general_practice_4/store/appstate.dart';
import 'package:general_practice_4/widgets/drawer_button_view_model.dart';

class DrawerButton extends StatelessWidget {
  final DrawerButtonModel buttonData;

  DrawerButton({
    @required this.buttonData,
  });

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, DrawerButtonViewModel>(
      converter: DrawerButtonViewModel.fromStore,
      builder: (BuildContext context, DrawerButtonViewModel viewModel) {
        return GestureDetector(
          onTap: () {
            viewModel.callback(buttonData.route);
          },
          child: Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: Container(
              color: Colors.lightGreen,
              height: 600.0,
              child: Center(
                child: Text(buttonData.name),
              ),
            ),
          ),
        );
      },
    );
  }
}
