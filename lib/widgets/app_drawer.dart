import 'package:flutter/material.dart';
import 'package:general_practice_4/res/const.dart';
import 'package:general_practice_4/widgets/drawer_button.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView.builder(
        itemCount: drawerButtons.length,
        itemBuilder: (BuildContext context, int index) {
          return DrawerButton(
            buttonData: drawerButtons[index],
          );
        },
      ),
    );
  }
}
