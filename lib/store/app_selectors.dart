import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:general_practice_4/store/appstate.dart';
import 'package:redux/redux.dart';

class NavigationSelector {
  static void Function(String route) navigateToNextPage(Store<AppState> store) {
    return (String route) => store.dispatch(NavigateToAction.push(route));
  }
}