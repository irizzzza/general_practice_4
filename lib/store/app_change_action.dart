import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'appstate.dart';

class AppNameChangedAction {
  final String name;

  AppNameChangedAction(this.name);
}



