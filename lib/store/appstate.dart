class AppState {
  final String name;
  const AppState(this.name);

  factory AppState.initial() => AppState(null);
}

class AppNameChangedAction {
  final String name;

  AppNameChangedAction(this.name);
}
