import 'package:flutter/material.dart';
import 'package:general_practice_4/pages/about_page.dart';
import 'package:general_practice_4/pages/cart_page.dart';
import 'package:general_practice_4/pages/detail_page.dart';
import 'package:general_practice_4/pages/home_page.dart';
import 'package:general_practice_4/res/const.dart';

class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  // endregion



  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HOME_PAGE:
        return _defaultRoute(
          settings: settings,
          page: HomePage(),
        );

      case ABOUT_PAGE:
        return _defaultRoute(
          settings: settings,
          page: AboutPage(),
        );

      case DETAIL_PAGE:
        return _defaultRoute(
          settings: settings,
          page: DetailPage(),
        );

      case CART_PAGE:
        return _defaultRoute(
          settings: settings,
          page: CartPage(),
        );


      default:
        return _defaultRoute(
          settings: settings,
          page: HomePage(),
        );


    }
  }

  static PageRoute _defaultRoute(
      {@required RouteSettings settings, @required Widget page}) {
    // return MaterialPageRoute(
    return PageRouteBuilder (
      settings: settings,
      pageBuilder: (BuildContext context, _, __) => page,
    );
  }
}