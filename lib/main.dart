import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:general_practice_4/helpers/route_helper.dart';
import 'package:general_practice_4/pages/home_page.dart';
import 'package:general_practice_4/store/appstate.dart';
import 'package:general_practice_4/utils/pop_navigator_observer.dart';
import 'package:redux/redux.dart';

import 'store/app_change_action.dart';

void main() {

  final store = new Store<AppState>(combineReducers<AppState>([]),
      initialState: AppState.initial(),
      middleware: [
        NavigationMiddleware<AppState>(),
      ]);

  runApp(MyApp(store));
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  final store;


  MyApp(this.store);

  PopNavigatorObserver popObserver = new PopNavigatorObserver();
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(

      store: store,
      child: MaterialApp(
        navigatorKey: NavigatorHolder.navigatorKey,
        navigatorObservers: [
          popObserver
        ],
        home: HomePage(),
        onGenerateRoute: (RouteSettings settings) => RouteHelper.instance.onGenerateRoute(settings),
      ),
    );
  }
}
