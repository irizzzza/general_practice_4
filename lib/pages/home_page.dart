import 'package:flutter/material.dart';
import 'package:general_practice_4/res/const.dart';
import 'package:general_practice_4/widgets/app_drawer.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: AppDrawer(),
      body: Center(
        child: Text(HOME_PAGE_BODY),
      ),
    );
  }
}
