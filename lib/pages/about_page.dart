import 'package:flutter/material.dart';
import 'package:general_practice_4/res/colors.dart';
import 'package:general_practice_4/res/const.dart';
import 'package:general_practice_4/widgets/app_drawer.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_S,
      appBar: AppBar(
      ),
      drawer: AppDrawer(),
      body: Center(
        child: Text(ABOUT_PAGE_BODY),
      ),
    );
  }
}
