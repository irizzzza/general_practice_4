import 'package:flutter/material.dart';
import 'package:general_practice_4/res/colors.dart';
import 'package:general_practice_4/res/const.dart';
import 'package:general_practice_4/widgets/app_drawer.dart';

class CartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_T,
      appBar: AppBar(),
    //  drawer: AppDrawer(),
      body: Center(
        child: Text(CART_PAGE_BODY),
      ),
    );
  }
}