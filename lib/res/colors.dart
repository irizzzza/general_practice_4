import 'package:flutter/material.dart';

const Color COLOR_S = const Color(0xff009EF8);
const Color COLOR_L = const Color(0xff272727);
const Color COLOR_T = const Color(0xffDA22FF);
const Color COLOR_W = const Color(0xff24C6DC);