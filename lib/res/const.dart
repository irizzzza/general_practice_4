
import 'package:general_practice_4/models/drawer_button_model.dart';

const String HOME_PAGE_BODY = 'Home page';
const String CART_PAGE_BODY = 'Cart page';
const String DETAIL_PAGE_BODY = 'Detail page';
const String ABOUT_PAGE_BODY = 'About page';


const String HOME_PAGE = '/home';
const String ABOUT_PAGE = '/about';
const String DETAIL_PAGE = '/detail';
const String CART_PAGE = '/cart';


 List<DrawerButtonModel> drawerButtons = [
   DrawerButtonModel(
     name: HOME_PAGE_BODY,
     route: HOME_PAGE,
   ),
   DrawerButtonModel(
     name: DETAIL_PAGE_BODY,
     route: DETAIL_PAGE,
   ),
   DrawerButtonModel(
     name: ABOUT_PAGE_BODY,
     route: ABOUT_PAGE,
   ),
   DrawerButtonModel(
     name: CART_PAGE_BODY,
     route: CART_PAGE,
   ),
 ];